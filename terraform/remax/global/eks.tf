/*data "aws_subnet_ids" "dev" {
  vpc_id = "${data.terraform_remote_state.dev_state.vpc_id}"
}*/

/*module "eks_master" {
  source = "../../modules/eks_master/"
  eks_name = "${var.account}_eks"
  region = "${var.region}"
  eks_subnets_id = ["subnet-080f5755c67959e53", "subnet-0095eafdc29d39c5d", "subnet-068c3d66dd9303c5a"]
  eks_master_sg_id = "sg-02ab4c5d1ec025323"
}*/



##################################
#  REPLACEMENT EKS TO COVER ALL ENVS
###################################
module "eks" {
  source = "../../modules/eks/"
  eks_name = "${var.eks_name}"
  region = "${var.region}"
  eks_subnets = ["${data.aws_subnet_ids.all.ids}"]
  tags = "${var.global_tags}"
  vpc_id = "${module.vpc.vpc_id}"
}