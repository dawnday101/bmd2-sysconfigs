resource "aws_vpc_peering_connection" "here2ops" {
  vpc_id = "${data.terraform_remote_state.state.vpc_id}"
  peer_owner_id = "${data.terraform_remote_state.ops_state.ops_owner_id}"
  peer_vpc_id = "${data.terraform_remote_state.ops_state.vpc_id}"
  peer_region   = "${var.ops_region}"
  #auto_accept = true
  tags = "${merge(map(
    "Name", "${var.account}_connection_${var.environment}",
    "Side", "Requester"),
    var.global_tags
  )}"
}

resource "aws_vpc_peering_connection_accepter" "ops2here" {
  provider = "aws.ops"
  #peer_owner_id = "${data.terraform_remote_state.state.crm_owner_id}"
  vpc_peering_connection_id = "${aws_vpc_peering_connection.here2ops.id}"
  auto_accept               = true
  tags = "${merge(map(
    "Name", "${var.account}_rconnection_${var.environment}",
    "Side", "Accepter"),
    var.global_tags
  )}"
}

resource "aws_route" "here2ops" {
  # ID of VPC 1 main route table.
  route_table_id = "${data.terraform_remote_state.state.route_table_ids[0]}"

  # CIDR block / IP range for VPC 2.
  destination_cidr_block = "10.1.0.0/16"

  # ID of VPC peering connection.
  vpc_peering_connection_id = "${aws_vpc_peering_connection.here2ops.id}"
}

resource "aws_route" "ops2here" {
  count = "${length(data.aws_subnet.all.*.cidr_block)}"
  provider = "aws.ops"
  # ID of VPC 1 main route table.
  route_table_id = "${data.terraform_remote_state.ops_state.route_table_ids[0]}"

  # CIDR block / IP range for VPC 2.
  destination_cidr_block = "${data.aws_subnet.all.*.cidr_block[count.index]}"

  # ID of VPC peering connection.
  vpc_peering_connection_id = "${aws_vpc_peering_connection.here2ops.id}"
  #vpc_peering_connection_id = "${aws_vpc_peering_connection_accepter.ops2crm.id}"
}

resource "aws_vpc_peering_connection" "here2hackcap" {
  vpc_id = "${data.terraform_remote_state.state.vpc_id}"
  peer_owner_id = "872227317957"
  peer_vpc_id = "vpc-0651f08335af69af5"
  peer_region   = "us-east-1"
  tags = "${merge(map(
    "Name", "hackcap_connection",
    "Side", "Requester"),
    var.global_tags
  )}"
}

resource "aws_route" "here2hackcap1" {
  # ID of VPC 1 main route table.
  route_table_id = "${data.terraform_remote_state.state.route_table_ids[0]}"
  # CIDR block / IP range for VPC 2.
  destination_cidr_block = "172.20.32.0/19"
  # ID of VPC peering connection.
  vpc_peering_connection_id = "${aws_vpc_peering_connection.here2hackcap.id}"
}

resource "aws_route" "here2hackcap2" {
  # ID of VPC 1 main route table.
  route_table_id = "${data.terraform_remote_state.state.route_table_ids[0]}"
  # CIDR block / IP range for VPC 2.
  destination_cidr_block = "172.20.64.0/19"
  # ID of VPC peering connection.
  vpc_peering_connection_id = "${aws_vpc_peering_connection.here2hackcap.id}"
}

resource "aws_route" "here2hackcap3" {
  # ID of VPC 1 main route table.
  route_table_id = "${data.terraform_remote_state.state.route_table_ids[0]}"
  # CIDR block / IP range for VPC 2.
  destination_cidr_block = "172.20.96.0/19"
  # ID of VPC peering connection.
  vpc_peering_connection_id = "${aws_vpc_peering_connection.here2hackcap.id}"
}