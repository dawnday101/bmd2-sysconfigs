terragrunt = {
  # Configure Terragrunt to automatically store tfstate files in an S3 bucket
  remote_state = {
    backend = "s3"

    config {
      encrypt        = true
      bucket         = "booj-terraform-state-remax"
      key            = "global/remax/terraform.tfstate"
      region         = "us-west-2"
      dynamodb_table = "terragrunt-locks-table-global-remax"
    }
  }
}