/*
resource "aws_elb" "kube" {
  name               = "a72aaf133d30f11e8912902a1b5344f0"
  availability_zones = ["us-west-2a", "us-west-2b", "us-west-2c"]

  access_logs {
    bucket        = "foo"
    bucket_prefix = "bar"
    interval      = 60
  }


  listener {
    instance_port     = 30117
    instance_protocol = "tcp"
    lb_port           = 80
    lb_protocol       = "tcp"
  }

  listener {
    instance_port      = 31544
    #instance_port     = 30117
    instance_protocol  = "tcp"
    lb_port            = 443
    lb_protocol        = "ssl"
    ssl_certificate_id = "arn:aws:acm:us-west-2:777333507603:certificate/c427be66-3d44-42dd-9ddc-47ed6e4d1658"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "TCP:30117"
    interval            = 10
  }

  cross_zone_load_balancing   = false
  idle_timeout                = 60
  connection_draining         = false
  connection_draining_timeout = 300

  tags {
    Name = "kube-elb"
    "kubernetes.io/cluster/remax_eks" = "owned"
    "kubernetes.io/service-name" = "gitlab-managed-apps/ingress-nginx-ingress-controller"


  }
}
*/
