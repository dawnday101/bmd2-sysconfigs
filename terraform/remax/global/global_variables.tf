variable "remax_access_key" {
  description = "AWS auth should pull from env variables"
  default = ""
}

variable "remax_secret_key" {
  description = "AWS auth should pull from env variables"
  default = ""
}

#### TerraGrunt global State
variable "global_terragrunt_state_key" {
  description = "Terragrunt State Key"
  default = "global/remax/terraform.tfstate"
}

variable "global_terragrunt_bucket" {
  description = "Terragrunt Bucket - Set to the AWS account/project name"
  default = "booj-terraform-state-remax"
}

variable "global_terragrunt_dynamodb_table" {
  description = "Terragrunt DynamoDB"
  default = "terragrunt-locks-table-global-remax"
}

#### TerraGrunt Dev State
variable "dev_terragrunt_state_key" {
  description = "Terragrunt State Key"
  default = "dev/remax/terraform.tfstate"
}

variable "dev_terragrunt_bucket" {
  description = "Terragrunt Bucket - Set to the AWS account/project name"
  default = "booj-terraform-state-remax"
}

variable "dev_terragrunt_dynamodb_table" {
  description = "Terragrunt DynamoDB"
  default = "terragrunt-locks-table-dev-remax"
}

#### TerraGrunt qa State
variable "qa_terragrunt_state_key" {
  description = "Terragrunt State Key"
  default = "qa/remax/terraform.tfstate"
}

variable "qa_terragrunt_bucket" {
  description = "Terragrunt Bucket - Set to the AWS account/project name"
  default = "booj-terraform-state-remax"
}

variable "qa_terragrunt_dynamodb_table" {
  description = "Terragrunt DynamoDB"
  default = "terragrunt-locks-table-qa-remax"
}

#### TerraGrunt iuat State
variable "iuat_terragrunt_state_key" {
  description = "Terragrunt State Key"
  default = "iuat/remax/terraform.tfstate"
}

variable "iuat_terragrunt_bucket" {
  description = "Terragrunt Bucket - Set to the AWS account/project name"
  default = "booj-terraform-state-remax"
}

variable "iuat_terragrunt_dynamodb_table" {
  description = "Terragrunt DynamoDB"
  default = "terragrunt-locks-table-iuat-remax"
}

#### TerraGrunt euat State
variable "euat_terragrunt_state_key" {
  description = "Terragrunt State Key"
  default = "euat/remax/terraform.tfstate"
}

variable "euat_terragrunt_bucket" {
  description = "Terragrunt Bucket - Set to the AWS account/project name"
  default = "booj-terraform-state-remax"
}

variable "euat_terragrunt_dynamodb_table" {
  description = "Terragrunt DynamoDB"
  default = "terragrunt-locks-table-euat-remax"
}

#### TerraGrunt Prod State
variable "prod_terragrunt_state_key" {
  description = "Terragrunt State Key"
  default = "prod/remax/terraform.tfstate"
}

variable "prod_terragrunt_bucket" {
  description = "Terragrunt Bucket - Set to the AWS account/project name"
  default = "booj-terraform-state-remax"
}

variable "prod_terragrunt_dynamodb_table" {
  description = "Terragrunt DynamoDB"
  default = "terragrunt-locks-table-prod-remax"
}

########     OPS INFO     ########
#### TerraGrunt Ops State
variable "ops_terragrunt_state_key" {
  description = "Terragrunt State Key"
  default = "prod/ops/terraform.tfstate"
}

variable "ops_terragrunt_bucket" {
  description = "Terragrunt Bucket - Set to the AWS account/project name"
  default = "booj-terraform-state-ops"
}

variable "ops_terragrunt_dynamodb_table" {
  description = "Terragrunt DynamoDB"
  default = "terragrunt-locks-table-prod-ops"
}

variable "ops_region" {
  description = "AWS Region"
  default = "us-west-2"
}

variable "ops_access_key" {
  description = "ops AWS auth should pull from env variables"
  default = ""
}

variable "ops_secret_key" {
  description = "ops AWS auth should pull from env variables"
  default = ""
}

variable "ops_subnet" {
  description = "ops subnet"
  default = "10.1.0.0/16"
}
############################################

variable "region" {
  description = "AWS Region"
  default = "us-west-2"
}

variable "owner" {
  description = "instance owner"
  default = "remax"
}

variable "account" {
  description = "AWS Account Assoc"
  default = "remax"
}

variable "product" {
  description = "Product Assoc"
  default = "remax"
}

variable "tags" {
  description = "A mapping of tags to assign to the resource"
  default     = {}
}

variable "rds_port" {
  description = "rds_port"
  default = "3306"
}

########################################

variable "cidr" {
  description = "subnets used"
  default = "10.10.0.0/16"
}

variable "public_subnets" {
  description = "public subnets"
  type = "list"
  default = ["10.10.0.0/20", "10.10.16.0/20", "10.10.32.0/20", "10.10.48.0/20", "10.10.64.0/20", "10.10.80.0/20", "10.10.96.0/20", "10.10.112.0/20", "10.10.128.0/20", "10.10.144.0/20", "10.10.160.0/20", "10.10.176.0/20", "10.10.192.0/20", "10.10.208.0/20"]
}

variable "dev_subnets" {
  type = "list"
  default = ["10.10.16.0/20", "10.10.32.0/20", "10.10.48.0/20"]
}

variable "qa_subnets" {
  type = "list"
  default = ["10.10.64.0/20", "10.10.80.0/20", "10.10.96.0/20"]
}

variable "prod_subnets" {
  type = "list"
  default = ["10.10.112.0/20", "10.10.128.0/20", "10.10.144.0/20"]
}

variable "iuat_subnets" {
  type = "list"
  default = ["10.10.160.0/20", "10.10.176.0/20"]
}

variable "euat_subnets" {
  type = "list"
  default = ["10.10.192.0/20", "10.10.208.0/20"]
}

variable "private_subnets" {
  description = "private subnets"
  type = "list"
  default = ["10.10.224.0/20", "10.10.240.0/20"]
}

variable "eks_name" {
  default = "eks"
}