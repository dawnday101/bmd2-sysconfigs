resource "aws_iam_user" "admin_notify_sns" {
  name = "admin_notify_sns_${var.environment}"
  path = "/"
}

resource "aws_iam_user_policy" "admin_notify_sns_policy" {
  name = "admin_notify_sns_policy_${var.environment}"
  user = "${aws_iam_user.admin_notify_sns.name}"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "*"
             ],
             "Resource": "${aws_sns_topic.admin_notify.arn}"
        }
    ]
}
EOF
}

resource "aws_iam_access_key" "admin_notify_sns" {
  user = "${aws_iam_user.admin_notify_sns.name}"
}

output "admin_notify_sns_key" {
  sensitive = true
  value = "${aws_iam_access_key.admin_notify_sns.id}"
}

output "admin_notify_sns_secret" {
  sensitive = true
  value = "${aws_iam_access_key.admin_notify_sns.secret}"
}

resource "aws_sns_topic" "admin_notify" {
  name_prefix = "${var.environment}-admin_notify-topic-"
  display_name = "${var.environment}-admin_notify-topic"
}