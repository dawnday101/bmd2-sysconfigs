output "config-map-aws-auth" {
  value = "${module.eks.config-map-aws-auth}"
}

output "kubeconfig" {
  value = "${module.eks.kubeconfig}"
}

/*output "orig-kubeconfig" {
  value = "${module.eks_master.kubeconfig}"
}*/

output "eks_node_instance_profile" {
  value = "${module.eks.eks_node_instance_profile}"
}

output "eks_sg_id" {
  value = "${module.eks.eks_sg_id}"
}

output "eks_node_sg_id" {
  value = "${module.eks.eks_node_sg_id}"
}

/*output "orig-eks_sg_id" {
  value = "${module.eks_master.eks_sg_id}"
}*/

####   VPC
output "subnets" {
  value = ["${module.vpc.public_subnets}"]
}

output "private_subnets" {
  value = ["${module.vpc.private_subnets}"]
}

output "database_subnets_id" {
  value = ["${module.vpc.database_subnets}"]
}

output "elasticache_subnets_id" {
  value = ["${module.vpc.public_subnets}"]
}

output "elasticache_subnet_group_id" {
  description = "ID of elasticache subnet group"
  value       = "${module.vpc.elasticache_subnet_group}"
}

output "subnets_id" {
  value = ["${module.vpc.public_subnets}"]
}

output "private_subnets_id" {
  value = ["${module.vpc.private_subnets}"]
}

output "subnet_cidr_blocks" {
  value = ["${data.aws_subnet.all.*.cidr_block}"]
}

output "dev_subnet_ids" {
  value = ["${data.aws_subnet.dev.*.id}"]
}

output "qa_subnet_ids" {
  value = ["${data.aws_subnet.qa.*.id}"]
}

output "prod_subnet_ids" {
  value = ["${data.aws_subnet.prod.*.id}"]
}

output "iuat_subnet_ids" {
  value = ["${data.aws_subnet.iuat.*.id}"]
}

output "euat_subnet_ids" {
  value = ["${data.aws_subnet.euat.*.id}"]
}

output "vpc_id" {
  value = "${module.vpc.vpc_id}"
}

output "vpc_igw_id" {
  value = "${module.vpc.igw_id}"
}

output "default_route_table_id" {
  value = "${module.vpc.default_route_table_id}"
}

output "default_vpc_main_route_table_id" {
  value = "${module.vpc.default_vpc_main_route_table_id}"
}

output "intra_route_table_ids" {
  value = "${module.vpc.intra_route_table_ids}"
}

output "nat_ids" {
  value = "${module.vpc.nat_ids}"
}

output "nat_public_ips" {
  value = "${module.vpc.nat_public_ips}"
}

output "natgw_ids" {
  value = "${module.vpc.natgw_ids}"
}

output "route_table_ids" {
  value = "${module.vpc.public_route_table_ids}"
}

output "vpc_main_route_table_id" {
  value = "${module.vpc.vpc_main_route_table_id}"
}

#### SNS
output "admin_notify_sns_arn" {
  value = "${aws_sns_topic.admin_notify.arn}"
}