#### Environment
variable "global_tags" {
  type = "map"
  default = {
    owner       = "remax"
    account     = "remax"
    email       = "devops@booj.com"
    product     = "remax"
    environment = "global"
    Terraform   = "true"
    creator     = "terraform"
  }
}

variable "environment" {
  description = "Development Environment"
  default = "global"
}

#### VPC
variable "vpc_name" {
  description = "VPC Name"
  default = "remax-vpc"
}

variable "enable_nat_gateway" {
  description = "enable_nat_gateway"
  default = false
}

variable "enable_vpn_gateway" {
  description = "enable_vpn_gateway"
  default = false
}

variable "database_subnet_group" {
  description = "create_database_subnet_group"
  default = false
}

variable "enable_dns_hostnames" {
  default = true
}

variable "enable_dns_support" {
  default = true
}