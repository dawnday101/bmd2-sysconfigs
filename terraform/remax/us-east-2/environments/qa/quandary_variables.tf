variable "quandary_rds_engine_version" {
  default = "5.7.22"
}

variable "quandary_rds_instance_class" {
  default = "db.t2.medium"
}

variable "quandary_rds_allocated_storage" {
  description = "rds_allocated_storage"
  default = 5
}

variable "quandary_rds_engine" {
  description = "rds_engine"
  default = "mysql"
}

variable "quandary_rds_storage_encrypted" {
  description = "storage_encrypted"
  default = false
}

variable "quandary_rds_password" {
  description = "rds_password"
}

variable "quandary_rds_retention" {
  default = 7
}