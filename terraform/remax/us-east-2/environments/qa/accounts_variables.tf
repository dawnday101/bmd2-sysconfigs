variable "accounts_rds_engine_version" {
  default = "5.7.22"
}

variable "accounts_rds_instance_class" {
  default = "db.t2.medium"
}

variable "accounts_rds_allocated_storage" {
  description = "rds_allocated_storage"
  default = 5
}

variable "accounts_rds_engine" {
  description = "rds_engine"
  default = "mysql"
}

variable "accounts_rds_storage_encrypted" {
  description = "storage_encrypted"
  default = false
}

variable "accounts_rds_password" {
  description = "rds_password"
}

variable "accounts_rds_retention" {
  default = 7
}