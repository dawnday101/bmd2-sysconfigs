resource "aws_iam_user" "quandary_sqs" {
  name = "quandary_sqs_${var.environment}"
  path = "/"
}

resource "aws_iam_user_policy" "quandary_sqs_policy" {
  name = "quandary_sqs_policy_${var.environment}"
  user = "${aws_iam_user.quandary_sqs.name}"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "*"
             ],
             "Resource": "${module.quandary_sqs.this_sqs_queue_arn}"
        }
    ]
}
EOF
}

resource "aws_iam_access_key" "quandary_sqs" {
  user = "${aws_iam_user.quandary_sqs.name}"
}

output "quandary_sqs_key" {
  sensitive = true
  value = "${aws_iam_access_key.quandary_sqs.id}"
}

output "quandary_sqs_secret" {
  sensitive = true
  value = "${aws_iam_access_key.quandary_sqs.secret}"
}

module "quandary_sqs" {
  source = "terraform-aws-modules/sqs/aws"

  name = "quandary_sqs_${var.environment}"


  policy = <<EOF
  {
       "Version": "2012-10-17",
       "Statement": [{
          "Effect": "Allow",
          "Action": "*",
          "Resource": "${aws_iam_user.quandary_sqs.arn}"
       }]
  }
  EOF

  tags = "${merge(map(
    "Name", "quandary_sqs_${var.environment}",
    "resource", "sqs"),
    var.region_tags
  )}"
}

resource "aws_iam_user" "crestone_sqs" {
  name = "crestone_sqs_${var.environment}"
  path = "/"
}

resource "aws_iam_user_policy" "crestone_sqs_policy" {
  name = "crestone_sqs_policy_${var.environment}"
  user = "${aws_iam_user.crestone_sqs.name}"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "*"
             ],
             "Resource": "${module.crestone_sqs.this_sqs_queue_arn}"
        }
    ]
}
EOF
}

resource "aws_iam_access_key" "crestone_sqs" {
  user = "${aws_iam_user.crestone_sqs.name}"
}

output "crestone_sqs_key" {
  sensitive = true
  value = "${aws_iam_access_key.crestone_sqs.id}"
}

output "crestone_sqs_secret" {
  sensitive = true
  value = "${aws_iam_access_key.crestone_sqs.secret}"
}

module "crestone_sqs" {
  source = "terraform-aws-modules/sqs/aws"
  name = "crestone_sqs_${var.environment}"
  policy = <<EOF
  {
       "Version": "2012-10-17",
       "Statement": [{
          "Effect": "Allow",
          "Action": "*",
          "Resource": "${aws_iam_user.crestone_sqs.arn}"
       }]
  }
  EOF
  tags = "${merge(map(
    "Name", "crestone_${var.environment}",
    "resource", "sqs"),
    var.region_tags
  )}"
}

module "default_sqs" {
  source = "terraform-aws-modules/sqs/aws"

  name = "sqs_${var.environment}"


  policy = <<EOF
  {
       "Version": "2012-10-17",
       "Statement": [{
          "Effect": "Allow",
          "Action": "*",
          "Resource": "*"
       }]
  }
  EOF

  tags = "${merge(map(
    "Name", "default_${var.environment}",
    "resource", "sqs"),
    var.region_tags
  )}"
}