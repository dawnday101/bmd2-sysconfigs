resource "aws_iam_user" "untracked_email_sns" {
  name = "untracked_email_sns_${var.environment}"
  path = "/"
}

resource "aws_iam_user_policy" "untracked_email_sns_policy" {
  name = "untracked_email_sns_policy_${var.environment}"
  user = "${aws_iam_user.untracked_email_sns.name}"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "*"
             ],
             "Resource": "${aws_sns_topic.untracked_email.arn}"
        }
    ]
}
EOF
}

resource "aws_iam_access_key" "untracked_email_sns" {
  user = "${aws_iam_user.untracked_email_sns.name}"
}

output "untracked_email_sns_key" {
  sensitive = true
  value = "${aws_iam_access_key.untracked_email_sns.id}"
}

output "untracked_email_sns_secret" {
  sensitive = true
  value = "${aws_iam_access_key.untracked_email_sns.secret}"
}

resource "aws_sns_topic" "untracked_email" {
  name_prefix = "${var.environment}-untracked-email-topic-"
  display_name = "${var.environment}-untracked-email-topic"
  policy = <<EOF
  {
       "Version": "2012-10-17",
       "Statement": [{
          "Effect": "Allow",
          "Action": "*",
          "Principals": "aws_iam_user.untracked_email_sns.arn"
       }]
  }
  EOF
}