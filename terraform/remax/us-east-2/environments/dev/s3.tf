resource "aws_s3_bucket" "log_bucket" {
  bucket_prefix = "${var.environment}-s3logs-"
  acl    = "log-delivery-write"

  lifecycle_rule {
    id      = "log"
    enabled = true
    prefix = "log/"

  tags = {
      "rule"      = "log"
      "autoclean" = "true"
    }

    transition {
      days          = 30
      storage_class = "STANDARD_IA" # or "ONEZONE_IA"
    }

    transition {
      days          = 60
      storage_class = "GLACIER"
    }

    expiration {
      days = 90
    }
  }
}

resource "aws_s3_bucket" "remaxweb" {
  bucket_prefix = "${var.environment}-remaxweb-"
  acl    = "private"

  versioning {
    enabled = true
  }

  logging {
    target_bucket = "${aws_s3_bucket.log_bucket.id}"
    target_prefix = "log/"
  }

tags = "${merge(map(
    "Name", "remaxweb",
    "Environment", "${var.environment}"),
    var.region_tags
  )}"
}

resource "aws_s3_bucket" "medialibrary" {
  bucket_prefix = "${var.environment}-medialibrary-"
  acl    = "private"

  versioning {
    enabled = true
  }

  logging {
    target_bucket = "${aws_s3_bucket.log_bucket.id}"
    target_prefix = "log/"
  }

tags = "${merge(map(
    "Name", "medialibrary",
    "Environment", "${var.environment}"),
    var.region_tags
  )}"
}

resource "aws_s3_bucket" "crestone-priv" {
  bucket_prefix = "${var.environment}-crestone-priv-"
  acl    = "private"

  versioning {
    enabled = true
  }

  logging {
    target_bucket = "${aws_s3_bucket.log_bucket.id}"
    target_prefix = "log/"
  }

tags = "${merge(map(
    "Name", "crestone-priv",
    "Environment", "${var.environment}"),
    var.region_tags
  )}"
}

resource "aws_iam_user" "quandary_images_s3" {
  name = "quandary_images_s3_${var.environment}"
  path = "/"
}

resource "aws_iam_user_policy" "quandary_images_s3_policy" {
  name = "quandary_images_s3_policy_${var.environment}"
  user = "${aws_iam_user.quandary_images_s3.name}"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "*"
             ],
             "Resource": "${aws_s3_bucket.quandary_images.arn}"
        }
    ]
}
EOF
}

resource "aws_iam_access_key" "quandary_images_s3" {
  user = "${aws_iam_user.quandary_images_s3.name}"
}

output "quandary_images_s3_key" {
  sensitive = true
  value = "${aws_iam_access_key.quandary_images_s3.id}"
}

output "quandary_images_s3_secret" {
  sensitive = true
  value = "${aws_iam_access_key.quandary_images_s3.secret}"
}

resource "aws_s3_bucket" "quandary_images" {
  bucket_prefix = "${var.environment}-quandary-images-"
  acl    = "private"
  versioning {
    enabled = true
  }
  logging {
    target_bucket = "${aws_s3_bucket.log_bucket.id}"
    target_prefix = "log/"
  }
  policy = <<EOF
{
       "Version": "2012-10-17",
       "Statement": [{
          "Effect": "Allow",
          "Action": "*",
          "Principals": "${aws_iam_user.quandary_images_s3.arn}"
       }]
}
  EOF
  tags = "${merge(map(
    "Name", "${var.environment}-quandary-images",
    "Environment", "${var.environment}"),
    var.region_tags
  )}"
}