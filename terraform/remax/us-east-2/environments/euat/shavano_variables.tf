variable "shavano_rds_engine_version" {
  default = "5.7.22"
}

variable "shavano_rds_instance_class" {
  default = "db.t2.medium"
}

variable "shavano_rds_allocated_storage" {
  description = "rds_allocated_storage"
  default = 5
}

variable "shavano_rds_engine" {
  description = "rds_engine"
  default = "mysql"
}

variable "shavano_rds_storage_encrypted" {
  description = "storage_encrypted"
  default = false
}

variable "shavano_rds_password" {
  description = "rds_password"
}

variable "shavano_rds_retention" {
  default = 7
}