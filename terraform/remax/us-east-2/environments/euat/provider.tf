provider "aws" {
  region     = "${var.region}"
}

provider "aws" {
  alias = "ops"
  region     = "${var.ops_region}"
  access_key = "${var.ops_access_key}"
  secret_key = "${var.ops_secret_key}"
}

data "terraform_remote_state" "state" {
  backend = "s3"
  config {
    bucket = "${var.euat_terragrunt_bucket}"
    key = "${var.euat_terragrunt_state_key}"
    region = "${var.region}"
  }
}

data "terraform_remote_state" "region_state" {
  backend = "s3"
  config {
    bucket = "${var.region_terragrunt_bucket}"
    key = "${var.region_terragrunt_state_key}"
    region = "${var.region}"
  }
}

data "terraform_remote_state" "ops_state" { #it won't accept "provider"
  backend = "s3"
  config {
    bucket = "${var.ops_terragrunt_bucket}"
    key = "${var.ops_terragrunt_state_key}"
    access_key = "${var.ops_access_key}"
    secret_key = "${var.ops_secret_key}"
    region = "${var.ops_region}"
  }
}

data "aws_caller_identity" "current" {}

data "aws_caller_identity" "ops" {
  provider = "aws.ops"
}
