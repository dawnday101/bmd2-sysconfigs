resource "aws_security_group" "elasticache_sg" {
  name        = "quandary_ec_${var.environment}_sg"
  description = "Security Group to allow traffic to ElastiCache"
  ingress {
    from_port = 6379
    to_port   = 6379
    protocol  = "tcp"
    self      = true
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  vpc_id = "${data.terraform_remote_state.region_state.vpc_id}"
  tags = "${var.region_tags}"
}

resource "aws_elasticache_subnet_group" "quandary_ec" {
  name        = "quandary-ec-${var.environment}-subnets"
  description = "Subnets the Redis instances can be place into."
  subnet_ids  = ["${data.terraform_remote_state.region_state.euat_subnet_ids}"]
}

resource "aws_elasticache_replication_group" "quandary_ec" {
  node_type                     = "${var.elasticache_instance_type}"
  number_cache_clusters         = 1
  port                          = 6379
  replication_group_description = "quandary-ec-${var.environment}"
  replication_group_id          = "quandary-ec-${var.environment}"
  security_group_ids            = ["${aws_security_group.elasticache_sg.id}", "${module.sg.this_security_group_id}"]
  subnet_group_name             = "${aws_elasticache_subnet_group.quandary_ec.name}"
}
