module "eks" {
  source = "../../../modules/eks/"
  eks_name = "${var.eks_name}"
  region = "${var.region}"
  eks_subnets = ["${data.aws_subnet_ids.all.ids}"]
  tags = "${var.region_tags}"
  vpc_id = "${module.vpc.vpc_id}"
}