module "sg" {
  source = "terraform-aws-modules/security-group/aws"
  name        = "region"
  description = "Default security group"
  vpc_id      = "${module.vpc.vpc_id}"
  ingress_cidr_blocks      = ["${var.cidr}"]
  ingress_rules = ["all-tcp", "all-udp", "all-icmp"]
  egress_cidr_blocks      = ["0.0.0.0/0"]
  egress_rules = ["all-tcp", "all-udp", "all-icmp"]
  tags = "${var.region_tags}"
}