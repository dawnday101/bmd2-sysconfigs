terragrunt = {
  # Configure Terragrunt to automatically store tfstate files in an S3 bucket
  remote_state = {
    backend = "s3"

    config {
      encrypt        = true
      bucket         = "booj-terraform-state-remax-us-east-2"
      key            = "region/remax/terraform.tfstate"
      region         = "us-east-2"
      dynamodb_table = "terragrunt-locks-table-region-remax-us-east-2"
    }
  }
}