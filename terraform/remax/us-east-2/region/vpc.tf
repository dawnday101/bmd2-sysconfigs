data "aws_security_group" "default" {
  vpc_id = "${module.vpc.vpc_id}"
  name   = "default"
}

data "aws_subnet_ids" "all" {
  vpc_id = "${module.vpc.vpc_id}"
}

data "aws_subnet" "all" {
  count = "${length(data.aws_subnet_ids.all.ids)}"
  id    = "${data.aws_subnet_ids.all.ids[count.index]}"
}

data "aws_subnet" "dev" {
  count = "${length(var.dev_subnets)}"
  cidr_block = "${var.dev_subnets[count.index]}"
}

data "aws_subnet" "qa" {
  count = "${length(var.qa_subnets)}"
  cidr_block = "${var.qa_subnets[count.index]}"
}

data "aws_subnet" "prod" {
  count = "${length(var.prod_subnets)}"
  cidr_block = "${var.prod_subnets[count.index]}"
}

data "aws_subnet" "iuat" {
  count = "${length(var.iuat_subnets)}"
  cidr_block = "${var.iuat_subnets[count.index]}"
}

data "aws_subnet" "euat" {
  count = "${length(var.euat_subnets)}"
  cidr_block = "${var.euat_subnets[count.index]}"
}

data "aws_subnet" "utility" { #the second private subnet repurposed
  cidr_block = "${var.private_subnets[1]}"
}

/*resource "aws_route" "nat" {
  route_table_id = "${module.vpc.public_route_table_ids[0]}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id = "${aws_nat_gateway.nat.id}"
}*/

resource "aws_route" "mls1" {
  route_table_id = "${module.vpc.public_route_table_ids[0]}"
  destination_cidr_block = "54.164.251.147/32"
  nat_gateway_id = "${aws_nat_gateway.mls.id}"
}

resource "aws_route" "mls2" {
  route_table_id = "${module.vpc.public_route_table_ids[0]}"
  destination_cidr_block = "35.169.76.75/32"
  nat_gateway_id = "${aws_nat_gateway.mls.id}"
}

resource "aws_route" "mls3" {
  route_table_id = "${module.vpc.public_route_table_ids[0]}"
  destination_cidr_block = "34.198.171.80/32"
  nat_gateway_id = "${aws_nat_gateway.mls.id}"
}

resource "aws_route" "test" {
  route_table_id = "${module.vpc.public_route_table_ids[0]}"
  destination_cidr_block = "8.26.65.116/32"
  nat_gateway_id = "${aws_nat_gateway.mls.id}"
}

resource "aws_route" "test2" {
  route_table_id = "${module.vpc.public_route_table_ids[0]}"
  destination_cidr_block = "104.28.0.0/16"
  nat_gateway_id = "${aws_nat_gateway.mls.id}"
}


module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "${var.vpc_name}"
  default_vpc_name = "${var.vpc_name}"
  cidr = "${var.cidr}"
  enable_dns_hostnames = "${var.enable_dns_hostnames}"
  enable_dns_support = "${var.enable_dns_support}"
  azs                 = ["${var.azs}"]
  public_subnets      = ["${var.public_subnets}"]
  private_subnets      = ["${var.private_subnets}"]
  create_database_subnet_group = "${var.database_subnet_group}"

  enable_nat_gateway = "${var.enable_nat_gateway}"
  one_nat_gateway_per_az = true
  enable_vpn_gateway = "${var.enable_vpn_gateway}"
  public_route_table_tags = "${merge(map(
    "Name", "${var.vpc_name}"),
    var.region_tags
  )}"
  tags = "${merge(map(
    "Name", "${var.vpc_name}",
    "kubernetes.io/cluster/${var.account}_eks", "shared",
    "kubernetes.io/cluster/eks", "shared"),
    var.region_tags
  )}"
}

resource "aws_eip" "mls" {
  vpc = true
}

resource "aws_nat_gateway" "mls" {
  allocation_id = "${aws_eip.mls.id}"
  subnet_id     = "${data.aws_subnet.utility.id}"
  tags = "${merge(var.region_tags, map("Name", "mls-nat"))}"
}

resource "aws_route" "utility" {
  route_table_id = "rtb-073fcd05370e14af5"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = "${module.vpc.igw_id}"
}