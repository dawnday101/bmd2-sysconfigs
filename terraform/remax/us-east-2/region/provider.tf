/*resource "aws_iam_account_alias" "alias" {
  #account_alias = "booj-${var.product}"
  account_alias = "remax"
}*/

provider "aws" {
  region     = "${var.region}"
}

provider "aws" {
  alias = "ops"
  region     = "${var.ops_region}"
  access_key = "${var.ops_access_key}"
  secret_key = "${var.ops_secret_key}"
}

terraform {
  backend "s3" {
    bucket = "booj-terraform-state-remax-us-east-2" #Revisit variables here
    key = "region/remax/terraform.tfstate"
    region = "us-east-2"
  }
}

data "terraform_remote_state" "state" {
  backend = "s3"
  config {
    bucket = "${var.region_terragrunt_bucket}"
    key = "${var.region_terragrunt_state_key}"
    region = "${var.region}"
  }
}

data "terraform_remote_state" "dev_state" { #it won't accept "provider"
  backend = "s3"
  config {
    bucket = "${var.dev_terragrunt_bucket}"
    key = "${var.dev_terragrunt_state_key}"
    access_key = "${var.remax_access_key}"
    secret_key = "${var.remax_secret_key}"
    region = "${var.region}"
  }
}

data "terraform_remote_state" "prod_state" { #it won't accept "provider"
  backend = "s3"
  config {
    bucket = "${var.prod_terragrunt_bucket}"
    key = "${var.prod_terragrunt_state_key}"
    access_key = "${var.remax_access_key}"
    secret_key = "${var.remax_secret_key}"
    region = "${var.region}"
  }
}

data "terraform_remote_state" "ops_state" { #it won't accept "provider"
  backend = "s3"
  config {
    bucket = "${var.ops_terragrunt_bucket}"
    key = "${var.ops_terragrunt_state_key}"
    access_key = "${var.ops_access_key}"
    secret_key = "${var.ops_secret_key}"
    region = "${var.ops_region}"
  }
}

data "aws_caller_identity" "current" {}