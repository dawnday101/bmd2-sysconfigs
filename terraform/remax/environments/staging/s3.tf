resource "aws_s3_bucket" "log_bucket" {
  bucket_prefix = "${var.environment}-s3logs-"
  acl    = "log-delivery-write"

  lifecycle_rule {
    id      = "log"
    enabled = true
    prefix = "log/"

  tags = {
      "rule"      = "log"
      "autoclean" = "true"
    }

    transition {
      days          = 30
      storage_class = "STANDARD_IA" # or "ONEZONE_IA"
    }

    transition {
      days          = 60
      storage_class = "GLACIER"
    }

    expiration {
      days = 90
    }
  }
}

resource "aws_s3_bucket" "remaxweb" {
  bucket_prefix = "${var.environment}-remaxweb-"
  acl    = "private"

  versioning {
    enabled = true
  }

  logging {
    target_bucket = "${aws_s3_bucket.log_bucket.id}"
    target_prefix = "log/"
  }

tags = "${merge(map(
    "Name", "remaxweb",
    "Environment", "${var.environment}"),
    var.global_tags
  )}"
}

resource "aws_s3_bucket" "medialibrary" {
  bucket_prefix = "${var.environment}-medialibrary-"
  acl    = "private"

  versioning {
    enabled = true
  }

  logging {
    target_bucket = "${aws_s3_bucket.log_bucket.id}"
    target_prefix = "log/"
  }

tags = "${merge(map(
    "Name", "medialibrary",
    "Environment", "${var.environment}"),
    var.global_tags
  )}"
}

resource "aws_s3_bucket" "crestone-priv" {
  bucket_prefix = "${var.environment}-crestone-priv-"
  acl    = "private"

  versioning {
    enabled = true
  }

  logging {
    target_bucket = "${aws_s3_bucket.log_bucket.id}"
    target_prefix = "log/"
  }

tags = "${merge(map(
    "Name", "crestone-priv",
    "Environment", "${var.environment}"),
    var.global_tags
  )}"
}