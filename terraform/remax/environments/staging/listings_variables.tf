variable "listings_rds_engine_version" {
  default = "5.7.22"
}

variable "listings_rds_instance_class" {
  default = "db.t2.medium"
}

variable "listings_rds_allocated_storage" {
  description = "rds_allocated_storage"
  default = 5
}

variable "listings_rds_engine" {
  description = "rds_engine"
  default = "mysql"
}

variable "listings_rds_storage_encrypted" {
  description = "storage_encrypted"
  default = false
}

variable "listings_rds_password" {
  description = "rds_password"
}

variable "listings_rds_retention" {
  default = 7
}