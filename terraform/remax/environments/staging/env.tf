terraform {
  backend "s3" {
    encrypt = true
    bucket = "booj-terraform-state-remax-euat" #Revisit variables here
    key = "euat/remax/terraform.tfstate"
    region = "us-west-2"
  }
}