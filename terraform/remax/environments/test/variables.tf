#### Environment
variable "global_tags" {
  type = "map"
  default = {
    owner       = "remax"
    account     = "remax"
    email       = "devops@booj.com"
    product     = "remax"
    environment = "iuat"
    Terraform   = "true"
    creator     = "terraform"
  }
}

variable "cert" {
  description = "SSL cert"
  default = "arn:aws:acm:us-west-2:777333507603:certificate/c427be66-3d44-42dd-9ddc-47ed6e4d1658"
}

variable "domain" {
  description = "domain"
  default = "remax.booj.io"
}

variable "environment" {
  description = "Environment"
  default = "iuat"
}

variable "azs" {
  type = "list"
  description = "availability zones"
  default = ["us-west-2b", "us-west-2c"]
}

variable "deploy_key" {
  description = "AWS Deployment Key"
  default = "deploy-key-iuat"
}

variable "monitoring" {
  description = "AWS monitoring"
  default = "true"
}

variable "sg_name" {
  description = "Secrity Group Prefix"
  default = "iuat_sg"
}

#### RDS
variable "rds_allocated_storage" {
  description = "rds_allocated_storage"
  default = 5
}

variable "rds_engine" {
  description = "rds_engine"
  default = "mysql"
}

variable "rds_engine_version" {
  description = "rds_engine_version"
  default = "5.7.19"
}

variable "rds_instance_class" {
  description = "rds_instance_class"
  default = "db.t2.small"
}

variable "rds_storage_encrypted" {
  description = "storage_encrypted"
  default = true
}

variable "rds_username" {
  description = "rds_username"
  default = "remaxiuat"
}

variable "remax_rds_password" {
  description = "rds_password"
}

variable "rds_multi_az" {
  description = "multi_az"
  default = false
}

####elasticache
variable "elasticache_failover" {
  description = "elasticache_failover"
  default = "true"
}

variable "elasticache_cluster_size" {
  description = "elasticache_cluster_size"
  default = "0"
}

variable "elasticache_instance_type" {
  description = "elasticache_instance_type"
  default = "cache.m3.medium"
}

variable "elasticache_engine_version" {
  description = "elasticache_engine_version"
  default = "3.2.4"
}

variable "elasticache_maintenance_window" {
  type = "string"
  description = "The window to perform maintenance in."
  default = "Sun:03:00-Sun:05:00"
}

variable "elasticache_availability_zone" {
  type = "string"
  description = "The availability zone to place the instance into."
  default = "us-west-2a"
}

variable "parameter_group_name" {
  description = "parameter_group_name"
  default = "default.redis5.0.cluster.on"
}

variable "elasticache_security_group_id" {
  description = "elasticache_security_group"
  default = ""
}

#### Elasticsearch
variable "elasticsearch_instance_type" {
  description = "elasticsearch_instance_type"
  default = "m3.medium.elasticsearch"
}

variable "elasticsearch_instance_count" {
  description = "elasticsearch_instance_count"
  default = "3"
}

variable "instance_count" {
  description = "instance count"
  default     = "0"
}

variable "instance_type" {
  description = "instance type"
  default     = "t2.micro"
}

variable "root_volume_size" {
  description = "volume size"
  default     = "15"
}

variable "ebs_volume_size" {
  description = "volume size"
  default     = "15"
}

##### EKS
variable "eks_instance_type" {
  description = "eks_instance_type"
  default = "t2.medium"
}

variable "eks_min_size" {
  description = "eks_min_size"
  default = "3"
}

variable "eks_max_size" {
  description = "eks_max_size"
  default = "15"
}