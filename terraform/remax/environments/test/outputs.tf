output "owner" {
  value = "${var.owner}"
}

output "account" {
  value = "${var.account}"
}

output "product" {
  value = "${var.product}"
}

output "environment" {
  value = "${var.environment}"
}

output "remax_owner_id" {
  value = "${data.aws_caller_identity.current.account_id}"
}

####    Security Groups
output "default_sg_id" {
  value = "${module.sg.this_security_group_id}"
}

output "ssh_sg_id" {
  value = "${module.ssh_sg.this_security_group_id}"
}

output "web_sg_id" {
  value = "${module.web_sg.this_security_group_id}"
}

####      EKS
output "eks_node_sg_id" {
  value = "${module.eks_sgs.eks_node_sg_id}"
}

output "eks_node_sg_arn" {
  value = "${module.eks_sgs.eks_node_sg_arn}"
}