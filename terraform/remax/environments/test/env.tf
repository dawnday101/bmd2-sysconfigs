terraform {
  backend "s3" {
    encrypt = true
    bucket = "booj-terraform-state-remax-iuat" #Revisit variables here
    key = "iuat/remax/terraform.tfstate"
    region = "us-west-2"
  }
}