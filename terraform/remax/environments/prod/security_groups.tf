module "sg" {
  source = "terraform-aws-modules/security-group/aws"
  name        = "default-${var.environment}"
  description = "Default security group"
  vpc_id      = "${data.terraform_remote_state.global_state.vpc_id}"

  ingress_cidr_blocks      = ["${var.prod_subnets}", "${var.ops_subnet}"]
  ingress_rules = ["all-all"]
  egress_cidr_blocks      = ["0.0.0.0/0"]
  egress_rules = ["all-all"]

  tags = "${var.global_tags}"
}

module "ssh_sg" {
  source = "terraform-aws-modules/security-group/aws"
  name        = "ssh-${var.environment}"
  description = "Default security group"
  vpc_id      = "${data.terraform_remote_state.global_state.vpc_id}"

  ingress_rules = ["ssh-tcp"]
  ingress_cidr_blocks      = ["0.0.0.0/0"]

  tags = "${var.global_tags}"
}

module "web_sg" {
  source = "terraform-aws-modules/security-group/aws"
  name        = "web-${var.environment}"
  description = "Default security group"
  vpc_id      = "${data.terraform_remote_state.global_state.vpc_id}"

  ingress_rules = ["http-80-tcp", "https-443-tcp"]
  ingress_cidr_blocks      = ["0.0.0.0/0"]

  tags = "${var.global_tags}"
}
