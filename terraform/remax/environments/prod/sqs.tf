module "quandary_transactional_sqs" {
  source = "terraform-aws-modules/sqs/aws"

  name = "quandary_transactional_sqs_${var.environment}"

  policy = <<EOF
  {
       "Version": "2012-10-17",
       "Statement": [{
          "Effect": "Allow",
          "Action": "*",
          "Resource": "*"
       }]
  }
  EOF

  tags = "${merge(map(
    "Name", "quandary_transactional_sqs_${var.environment}",
    "resource", "sqs"),
    var.global_tags
  )}"
}