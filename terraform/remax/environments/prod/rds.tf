
resource "aws_secretsmanager_secret" "rds" {
  name = "${var.product}-rds-${var.environment}"
  tags = "${merge(map(
    "Name", "${var.product}_rds_${var.environment}"),
    var.global_tags
  )}"
}

resource "aws_secretsmanager_secret_version" "rds" {
  secret_id     = "${var.product}-rds-${var.environment}"
  version_stages = ["AWSCURRENT"]
  secret_string = "${var.remax_rds_password}"
}


data "aws_secretsmanager_secret_version" "rds" {
  secret_id = "${var.product}-rds-${var.environment}"
  depends_on = ["aws_secretsmanager_secret.rds"]
}

resource "aws_secretsmanager_secret" "deals_rds" {
  name = "deals-rds-${var.environment}"
  tags = "${merge(map(
    "Name", "deals_rds_${var.environment}"),
    var.global_tags
  )}"
}

resource "aws_secretsmanager_secret_version" "deals_rds" {
  secret_id     = "deals-rds-${var.environment}"
  version_stages = ["AWSCURRENT"]
  secret_string = "${var.deals_rds_password}"
}


data "aws_secretsmanager_secret_version" "deals_rds" {
  secret_id = "deals-rds-${var.environment}"
  depends_on = ["aws_secretsmanager_secret.deals_rds"]
}

#####
# DB Modifications to the database are applied during the maintenance window unless you set "apply_immediately"
#####

resource "aws_db_option_group" "default" {
  name_prefix              = "default-${var.environment}-"
  option_group_description = "default-${var.environment}"
  engine_name              = "mysql"
  major_engine_version     = "5.7"
  option = [
    {
      option_name = "MARIADB_AUDIT_PLUGIN"

      option_settings = [
        {
          name  = "SERVER_AUDIT_EVENTS"
          value = "CONNECT"
        },
        {
          name  = "SERVER_AUDIT_FILE_ROTATIONS"
          value = "37"
        },
      ]
    },
  ]
  tags = "${var.global_tags}"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_db_parameter_group" "default" {
  name_prefix = "default-${var.environment}-"
  description = "default-${var.environment}"
  family      = "mysql5.7"
  parameter = [
    {
      name = "log_bin_trust_function_creators"
      value = true
    }
  ]
  tags = "${var.global_tags}"
  lifecycle {
    create_before_destroy = true
  }
}

/*module "rds" {
  source = "terraform-aws-modules/rds/aws"

  identifier = "${var.product}-rds-${var.environment}"

  # All available versions: http://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_MySQL.html#MySQL.Concepts.VersionMgmt
  engine            = "${var.rds_engine}"
  engine_version    = "${var.rds_engine_version}"
  instance_class    = "${var.rds_instance_class}"
  allocated_storage = "${var.rds_allocated_storage}"
  storage_encrypted = "${var.rds_storage_encrypted}"
  multi_az          = "${var.rds_multi_az}"



  # kms_key_id        = "arm:aws:kms:<region>:<accound id>:key/<kms key id>"
  name     = "${var.product}_rds_${var.environment}"
  username = "${var.rds_username}"
  password = "${data.aws_secretsmanager_secret_version.rds.secret_string}"
  port     = "${var.rds_port}"

  vpc_security_group_ids = ["${module.sg.this_security_group_id}"]

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  # disable backups to create DB faster
  backup_retention_period = 7

  tags = "${merge(map(
    "Name", "${var.product}_rds_${var.environment}"),
    var.global_tags
  )}"

  # DB subnet group
  subnet_ids = ["${data.terraform_remote_state.global_state.prod_subnet_ids}"]

  # DB parameter group
  family = "mysql5.7"

  # DB option group
  major_engine_version = "5.7"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = "remaxdb"

  create_db_parameter_group = false
  parameter_group_name = "${aws_db_parameter_group.default.id}"

  create_db_option_group = false
  option_group_name = "${aws_db_option_group.default.id}"

}*/

module "deals_rds" {
  source = "github.com/kanewinter/terraform-aws-rds"

  identifier = "deals-rds-${var.environment}"

  # All available versions: http://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_MySQL.html#MySQL.Concepts.VersionMgmt
  engine            = "${var.deals_rds_engine}"
  engine_version    = "${var.deals_rds_engine_version}"
  instance_class    = "${var.deals_rds_instance_class}"
  allocated_storage = "${var.deals_rds_allocated_storage}"
  storage_encrypted = "${var.deals_rds_storage_encrypted}"
  multi_az          = "${var.rds_multi_az}"



  # kms_key_id        = "arm:aws:kms:<region>:<accound id>:key/<kms key id>"
  name     = "deals_rds_${var.environment}"
  username = "deals_${var.environment}"
  password = "${data.aws_secretsmanager_secret_version.deals_rds.secret_string}"
  port     = "${var.rds_port}"

  vpc_security_group_ids = ["${module.sg.this_security_group_id}"]

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  # disable backups to create DB faster
  backup_retention_period = "${var.deals_rds_retention}"

  tags = "${merge(map(
    "Name", "deals_rds_${var.environment}"),
    var.global_tags
  )}"

  # DB subnet group
  subnet_ids = ["${data.terraform_remote_state.global_state.prod_subnet_ids}"]

  # DB parameter group
  family = "mysql5.7"

  # DB option group
  major_engine_version = "5.7"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = "dealsdb"

  create_db_parameter_group = false
  parameter_group_name = "${aws_db_parameter_group.default.id}"

  create_db_option_group = false
  option_group_name = "${aws_db_option_group.default.id}"

}