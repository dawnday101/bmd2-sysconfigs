terraform {
  backend "s3" {
    encrypt = true
    bucket = "booj-terraform-state-remax-prod" #Revisit variables here
    key = "prod/remax/terraform.tfstate"
    region = "us-west-2"
  }
}