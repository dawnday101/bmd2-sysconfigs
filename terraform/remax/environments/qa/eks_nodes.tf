module "eks_sgs" {
  source = "../../../modules/eks_sgs/"
  eks_name = "${var.eks_name}"
  env = "${var.environment}"
  vpc_id = "${data.terraform_remote_state.global_state.vpc_id}"
  cidr = ["${var.qa_subnets}"]
  eks_sg_id = "${data.terraform_remote_state.global_state.eks_sg_id}"
}

module "eks_nodes" {
  source = "../../../modules/eks_cluster/"
  eks_name = "${var.eks_name}"
  env = "${var.environment}"
  account = "${var.account}"
  eks_instance_type = "${var.eks_instance_type}"
  deploy_key = "${var.deploy_key}"
  eks_subnets_id = ["${data.terraform_remote_state.global_state.qa_subnet_ids}"]
  default_sg_id = "${module.sg.this_security_group_id}"
  eks_min_size = "${var.eks_min_size}"
  eks_max_size = "${var.eks_max_size}"
  eks_node_sg_id = ["${module.eks_sgs.eks_node_sg_id}", "${data.terraform_remote_state.global_state.eks_node_sg_id}"]
  eks_node_instance_profile = "${data.terraform_remote_state.global_state.eks_node_instance_profile}"
}