terraform {
  backend "s3" {
    encrypt = true
    bucket = "booj-terraform-state-remax-qa" #Revisit variables here
    key = "qa/remax/terraform.tfstate"
    region = "us-west-2"
  }
}