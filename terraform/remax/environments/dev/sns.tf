resource "aws_iam_user" "untracked_email_sns" {
  name = "untracked_email_sns_${var.environment}"
  path = "/"
}

resource "aws_iam_user_policy" "untracked_email_sns_policy" {
  name = "untracked_email_sns_policy_${var.environment}"
  user = "${aws_iam_user.untracked_email_sns.name}"
  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "*"
             ],
             "Resource": "arn:aws:sns:${var.region}:${data.aws_caller_identity.current.account_id}:*"
        }
    ]
}
EOF
}

resource "aws_iam_access_key" "untracked_email_sns" {
  user = "${aws_iam_user.untracked_email_sns.name}"
}

output "untracked_email_sns_key" {
  sensitive = true
  value = "${aws_iam_access_key.untracked_email_sns.id}"
}

output "untracked_email_sns_secret" {
  sensitive = true
  value = "${aws_iam_access_key.untracked_email_sns.secret}"
}

/*
resource "aws_sns_topic" "untracked_email" {
  name_prefix = "${var.environment}-untracked-email-topic-"
  display_name = "${var.environment}-untracked-email-topic"
}

resource "aws_sns_topic_policy" "untracked_email" {
  arn = "${aws_sns_topic.untracked_email.arn}"
  policy = "${data.aws_iam_policy_document.untracked_email.json}"
}

data "aws_iam_policy_document" "untracked_email" {
  policy_id = "__default_policy_ID"
  statement {
    actions = [
      "SNS:Subscribe",
      "SNS:SetTopicAttributes",
      "SNS:RemovePermission",
      "SNS:Receive",
      "SNS:Publish",
      "SNS:ListSubscriptionsByTopic",
      "SNS:GetTopicAttributes",
      "SNS:DeleteTopic",
      "SNS:ListTopics",
      "SNS:AddPermission",
    ]
    effect = "Allow"
    principals {
      type        = "AWS"
      identifiers = ["${aws_iam_user.untracked_email_sns.arn}"]
    }
    resources = [
      "${aws_sns_topic.untracked_email.arn}",
    ]
    sid = "__default_statement_ID"
  }
}*/
