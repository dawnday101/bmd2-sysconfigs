#####
# DB Modifications to the database are applied during the maintenance window unless you set "apply_immediately"
#####
resource "aws_db_option_group" "default" {
  name_prefix              = "default-${var.environment}-"
  option_group_description = "default-${var.environment}"
  engine_name              = "mysql"
  major_engine_version     = "5.7"
  option = [
    {
      option_name = "MARIADB_AUDIT_PLUGIN"

      option_settings = [
        {
          name  = "SERVER_AUDIT_EVENTS"
          value = "CONNECT"
        },
        {
          name  = "SERVER_AUDIT_FILE_ROTATIONS"
          value = "37"
        },
      ]
    },
  ]
  tags = "${var.global_tags}"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_db_parameter_group" "default" {
  name_prefix = "default-${var.environment}-"
  description = "default-${var.environment}"
  family      = "mysql5.7"
  parameter = [
    {
      name = "log_bin_trust_function_creators"
      value = true
    }
  ]
  tags = "${var.global_tags}"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_db_subnet_group" "default" {
  name_prefix = "default-${var.environment}-"
  subnet_ids = ["${data.terraform_remote_state.global_state.dev_subnet_ids}"]
  tags = "${var.global_tags}"
}

module "deals_rds" {
  #version = "1.20.0"
  source = "github.com/kanewinter/terraform-aws-rds"
  identifier = "deals-rds-${var.environment}"

  # All available versions: http://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_MySQL.html#MySQL.Concepts.VersionMgmt
  engine            = "${var.deals_rds_engine}"
  engine_version    = "${var.deals_rds_engine_version}"
  instance_class    = "${var.deals_rds_instance_class}"
  allocated_storage = "${var.deals_rds_allocated_storage}"
  storage_encrypted = "${var.deals_rds_storage_encrypted}"
  multi_az          = "${var.rds_multi_az}"
  
  # kms_key_id        = "arm:aws:kms:<region>:<accound id>:key/<kms key id>"
  name     = "deals_rds_${var.environment}"
  username = "deals_${var.environment}"
  password = "${var.deals_rds_password}"
  port     = "${var.rds_port}"
  vpc_security_group_ids = ["${data.terraform_remote_state.state.default_sg_id}"]
  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  # disable backups to create DB faster
  backup_retention_period = "${var.deals_rds_retention}"

  tags = "${merge(map(
    "Name", "deals_rds_${var.environment}"),
    var.global_tags
  )}"

  # DB subnet group
  create_db_subnet_group = true
  subnet_ids = ["${data.terraform_remote_state.global_state.dev_subnet_ids}"]

  # DB parameter group
  family = "mysql5.7"

  # DB option group
  major_engine_version = "5.7"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = "dealsdb"

  create_db_parameter_group = false
  parameter_group_name = "${aws_db_parameter_group.default.id}"

  create_db_option_group = false
  option_group_name = "${aws_db_option_group.default.id}"
}

module "accounts_rds" {
  source = "github.com/kanewinter/terraform-aws-rds"
  identifier = "accounts-rds-${var.environment}"

  # All available versions: http://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_MySQL.html#MySQL.Concepts.VersionMgmt
  engine            = "${var.accounts_rds_engine}"
  engine_version    = "${var.accounts_rds_engine_version}"
  instance_class    = "${var.accounts_rds_instance_class}"
  allocated_storage = "${var.accounts_rds_allocated_storage}"
  storage_encrypted = "${var.accounts_rds_storage_encrypted}"
  multi_az          = "${var.rds_multi_az}"
  
  # kms_key_id        = "arm:aws:kms:<region>:<accound id>:key/<kms key id>"
  name     = "accounts_rds_${var.environment}"
  username = "accounts_${var.environment}"
  password = "${var.accounts_rds_password}"
  port     = "${var.rds_port}"
  vpc_security_group_ids = ["${data.terraform_remote_state.state.default_sg_id}"]
  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  # disable backups to create DB faster
  backup_retention_period = "${var.accounts_rds_retention}"

  tags = "${merge(map(
    "Name", "accounts_rds_${var.environment}"),
    var.global_tags
  )}"

  # DB subnet group
  create_db_subnet_group = true
  subnet_ids = ["${data.terraform_remote_state.global_state.dev_subnet_ids}"]

  # DB parameter group
  family = "mysql5.7"

  # DB option group
  major_engine_version = "5.7"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = "accountsdb"

  create_db_parameter_group = false
  parameter_group_name = "${aws_db_parameter_group.default.id}"

  create_db_option_group = false
  option_group_name = "${aws_db_option_group.default.id}"
}

module "listings_rds" {
  source = "github.com/kanewinter/terraform-aws-rds"
  identifier = "listings-rds-${var.environment}"

  # All available versions: http://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_MySQL.html#MySQL.Concepts.VersionMgmt
  engine            = "${var.listings_rds_engine}"
  engine_version    = "${var.listings_rds_engine_version}"
  instance_class    = "${var.listings_rds_instance_class}"
  allocated_storage = "${var.listings_rds_allocated_storage}"
  storage_encrypted = "${var.listings_rds_storage_encrypted}"
  multi_az          = "${var.rds_multi_az}"
  
  # kms_key_id        = "arm:aws:kms:<region>:<accound id>:key/<kms key id>"
  name     = "listings_rds_${var.environment}"
  username = "listings_${var.environment}"
  password = "${var.listings_rds_password}"
  port     = "${var.rds_port}"
  vpc_security_group_ids = ["${data.terraform_remote_state.state.default_sg_id}"]
  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  # disable backups to create DB faster
  backup_retention_period = "${var.listings_rds_retention}"

  tags = "${merge(map(
    "Name", "listings_rds_${var.environment}"),
    var.global_tags
  )}"

  # DB subnet group
  create_db_subnet_group = true
  subnet_ids = ["${data.terraform_remote_state.global_state.dev_subnet_ids}"]

  # DB parameter group
  family = "mysql5.7"

  # DB option group
  major_engine_version = "5.7"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = "listingsdb"

  create_db_parameter_group = false
  parameter_group_name = "${aws_db_parameter_group.default.id}"

  create_db_option_group = false
  option_group_name = "${aws_db_option_group.default.id}"
}

module "cms_rds" {
  source = "github.com/kanewinter/terraform-aws-rds"
  identifier = "cms-rds-${var.environment}"

  # All available versions: http://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_MySQL.html#MySQL.Concepts.VersionMgmt
  engine            = "${var.cms_rds_engine}"
  engine_version    = "${var.cms_rds_engine_version}"
  instance_class    = "${var.cms_rds_instance_class}"
  allocated_storage = "${var.cms_rds_allocated_storage}"
  storage_encrypted = "${var.cms_rds_storage_encrypted}"
  multi_az          = "${var.rds_multi_az}"
  
  # kms_key_id        = "arm:aws:kms:<region>:<accound id>:key/<kms key id>"
  name     = "cms_rds_${var.environment}"
  username = "cms_${var.environment}"
  password = "${var.cms_rds_password}"
  port     = "${var.rds_port}"
  vpc_security_group_ids = ["${data.terraform_remote_state.state.default_sg_id}"]
  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  # disable backups to create DB faster
  backup_retention_period = "${var.cms_rds_retention}"

  tags = "${merge(map(
    "Name", "cms_rds_${var.environment}"),
    var.global_tags
  )}"

  # DB subnet group
  create_db_subnet_group = true
  subnet_ids = ["${data.terraform_remote_state.global_state.dev_subnet_ids}"]

  # DB parameter group
  family = "mysql5.7"

  # DB option group
  major_engine_version = "5.7"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = "cmsdb"

  create_db_parameter_group = false
  parameter_group_name = "${aws_db_parameter_group.default.id}"

  create_db_option_group = false
  option_group_name = "${aws_db_option_group.default.id}"
}

module "quandary_rds" {
  source = "github.com/kanewinter/terraform-aws-rds"
  identifier = "quandary-rds-${var.environment}"

  # All available versions: http://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_MySQL.html#MySQL.Concepts.VersionMgmt
  engine            = "${var.quandary_rds_engine}"
  engine_version    = "${var.quandary_rds_engine_version}"
  instance_class    = "${var.quandary_rds_instance_class}"
  allocated_storage = "${var.quandary_rds_allocated_storage}"
  storage_encrypted = "${var.quandary_rds_storage_encrypted}"
  multi_az          = "${var.rds_multi_az}"
  
  # kms_key_id        = "arm:aws:kms:<region>:<accound id>:key/<kms key id>"
  name     = "quandary_rds_${var.environment}"
  username = "quandary_${var.environment}"
  password = "${var.quandary_rds_password}"
  port     = "${var.rds_port}"
  vpc_security_group_ids = ["${data.terraform_remote_state.state.default_sg_id}"]
  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  # disable backups to create DB faster
  backup_retention_period = "${var.quandary_rds_retention}"

  tags = "${merge(map(
    "Name", "quandary_rds_${var.environment}"),
    var.global_tags
  )}"

  # DB subnet group
  create_db_subnet_group = true
  subnet_ids = ["${data.terraform_remote_state.global_state.dev_subnet_ids}"]

  # DB parameter group
  family = "mysql5.7"

  # DB option group
  major_engine_version = "5.7"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = "quandarydb"

  create_db_parameter_group = false
  parameter_group_name = "${aws_db_parameter_group.default.id}"

  create_db_option_group = false
  option_group_name = "${aws_db_option_group.default.id}"
}

###########################################################
######### NEW DBS USE DEFAULT SUBNET GROUP!!!   ###########
###########################################################
module "shavano_rds" {
  source = "github.com/kanewinter/terraform-aws-rds"
  identifier = "shavano-rds-${var.environment}"

  # All available versions: http://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_MySQL.html#MySQL.Concepts.VersionMgmt
  engine            = "${var.shavano_rds_engine}"
  engine_version    = "${var.shavano_rds_engine_version}"
  instance_class    = "${var.shavano_rds_instance_class}"
  allocated_storage = "${var.shavano_rds_allocated_storage}"
  storage_encrypted = "${var.shavano_rds_storage_encrypted}"
  multi_az          = "${var.rds_multi_az}"

  # kms_key_id        = "arm:aws:kms:<region>:<accound id>:key/<kms key id>"
  name     = "shavano_rds_${var.environment}"
  username = "shavano_${var.environment}"
  password = "${var.shavano_rds_password}"
  port     = "${var.rds_port}"
  vpc_security_group_ids = ["${data.terraform_remote_state.state.default_sg_id}"]
  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  # disable backups to create DB faster
  backup_retention_period = "${var.shavano_rds_retention}"

  tags = "${merge(map(
    "Name", "shavano_rds_${var.environment}"),
    var.global_tags
  )}"

  # DB subnet group
  create_db_subnet_group = false
  db_subnet_group_name = "${aws_db_subnet_group.default.id}"

  # DB parameter group
  family = "mysql5.7"

  # DB option group
  major_engine_version = "5.7"

  # Snapshot name upon DB deletion
  final_snapshot_identifier = "shavanodb"

  create_db_parameter_group = false
  parameter_group_name = "${aws_db_parameter_group.default.id}"

  create_db_option_group = false
  option_group_name = "${aws_db_option_group.default.id}"
}