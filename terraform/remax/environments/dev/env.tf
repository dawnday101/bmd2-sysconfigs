terraform {
  backend "s3" {
    encrypt = true
    bucket = "booj-terraform-state-remax-dev" #Revisit variables here
    key = "dev/remax/terraform.tfstate"
    region = "us-west-2"
  }
}