resource "aws_cloudwatch_log_group" "elasticsearch" {
  name = "elasticsearch_${var.environment}"
  retention_in_days = "180"
}

resource "aws_cloudwatch_log_resource_policy" "elasticsearch" {
  policy_name = "elasticsearch_${var.environment}"
  policy_document = <<CONFIG
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "es.amazonaws.com"
      },
      "Action": [
        "logs:PutLogEvents",
        "logs:PutLogEventsBatch",
        "logs:CreateLogStream"
      ],
      "Resource": "arn:aws:logs:*"
    }
  ]
}
CONFIG
}

resource "aws_iam_service_linked_role" "es" {
  aws_service_name = "es.amazonaws.com"
}

resource "aws_security_group" "elasticsearch" {
  name        = "elasticsearch_${var.environment}_sg"
  description = "Security Group to allow traffic to ElasticSearch"

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"
    self      = true
  }

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    self      = true
  }

  ingress {
    from_port = 9200
    to_port   = 9200
    protocol  = "tcp"
    self      = true
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${data.terraform_remote_state.global_state.vpc_id}"

  tags = "${var.global_tags}"
}

resource "aws_elasticsearch_domain" "crestone" {
  domain_name           = "crestone-elasticsearch-${var.environment}"
  elasticsearch_version = "6.4"
  cluster_config {
    #####Possible Instance types:
    #[i3.2xlarge.elasticsearch, i3.4xlarge.elasticsearch, m3.large.elasticsearch, r4.16xlarge.elasticsearch, t2.micro.elasticsearch, m4.large.elasticsearch, d2.2xlarge.elasticsearch, i3.8xlarge.elasticsearch, i3.large.elasticsearch, d2.4xlarge.elasticsearch, t2.small.elasticsearch, c4.2xlarge.elasticsearch, c4.4xlarge.elasticsearch, d2.8xlarge.elasticsearch, m3.medium.elasticsearch, c4.8xlarge.elasticsearch, c4.large.elasticsearch, c4.xlarge.elasticsearch, d2.xlarge.elasticsearch, t2.medium.elasticsearch, i3.xlarge.elasticsearch, i2.xlarge.elasticsearch, r3.2xlarge.elasticsearch, r4.2xlarge.elasticsearch, m4.10xlarge.elasticsearch, r3.4xlarge.elasticsearch, m4.xlarge.elasticsearch, r4.4xlarge.elasticsearch, m3.xlarge.elasticsearch, i3.16xlarge.elasticsearch, m3.2xlarge.elasticsearch, r3.8xlarge.elasticsearch, r3.large.elasticsearch, m4.2xlarge.elasticsearch, r4.8xlarge.elasticsearch, r4.xlarge.elasticsearch, r4.large.elasticsearch, i2.2xlarge.elasticsearch, r3.xlarge.elasticsearch, m4.4xlarge.elasticsearch]
    instance_type = "t2.small.elasticsearch"
    instance_count = "2"
    zone_awareness_enabled = true
  }

  vpc_options {
    security_group_ids = ["${aws_security_group.elasticsearch.id}", "${module.sg.this_security_group_id}"]
    subnet_ids = ["${data.terraform_remote_state.global_state.dev_subnet_ids[0]}", "${data.terraform_remote_state.global_state.dev_subnet_ids[1]}"]
  }

  ebs_options {
    ebs_enabled = true
    volume_size = "20"
  }

  snapshot_options {
    automated_snapshot_start_hour = 1
  }

  log_publishing_options {
    cloudwatch_log_group_arn = "${aws_cloudwatch_log_group.elasticsearch.arn}"
    log_type                 = "INDEX_SLOW_LOGS"
  }

  tags = "${merge(var.global_tags, map("Name", "elasticsearch"))}"
}

resource "aws_elasticsearch_domain_policy" "this" {
  domain_name = "${aws_elasticsearch_domain.crestone.domain_name}"

  access_policies = <<POLICIES
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "*"
        ]
      },
      "Action": [
        "es:*"
      ],
      "Resource": "${aws_elasticsearch_domain.crestone.arn}/*"
    }
  ]
}
POLICIES
}